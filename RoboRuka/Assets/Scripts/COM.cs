﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.IO.Ports;

public class COM
{
        public SerialPort serialPort;

        public COM(string port)
        {
            serialPort = new SerialPort(port,
                9600,
                Parity.None,
                8,
                StopBits.One);
        }
        public void SendData(int data)
        {
            try
            {
                serialPort.Open();
                serialPort.Write(data.ToString());
                serialPort.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
}
