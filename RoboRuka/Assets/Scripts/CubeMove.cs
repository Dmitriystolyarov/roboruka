﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO.Ports;
using TMPro;

public class CubeMove : MonoBehaviour
{
    public Rigidbody rb;// свойства объекта
    public float sidewayForce = 50f;// сила движения вперед
    public float pos_x; // текущая позиция кубика по x(x, y, z)
    public float pos_y; // текущая позиция кубика по y(x, y, z)
    public int Degree_x;// определили число градусов по x
    public int Degree_y;// определили число градусов по y
    public string str; 
    //SerialPort port;
    SerialPort sp = new SerialPort("COM4", 9600);

    void Start()
    {
        sp.Open();
        sp.ReadTimeout = 1;
    }

    void Update()
    {
        // добавляем силу смещения вперед
        //rb.AddForce(0, 0, 0);

        if (Input.GetKey("d"))
        {
            rb.AddForce(sidewayForce * Time.deltaTime, 0, 0, ForceMode.VelocityChange);
        }
        if (Input.GetKey("a"))
        {
            rb.AddForce(-sidewayForce * Time.deltaTime, 0, 0, ForceMode.VelocityChange);
        }

        if (Input.GetKey("w"))
        {
            rb.AddForce(0, sidewayForce * Time.deltaTime, 0, ForceMode.VelocityChange);
        }
        if (Input.GetKey("s"))
        {
            rb.AddForce(0, -sidewayForce * Time.deltaTime, 0, ForceMode.VelocityChange);
        }

        pos_x = 10*transform.position.x; // умножили на 10, чтобы работать с более крупными числами по оси x
        Degree_x = (int)pos_x + 90;

        pos_y = 10 * transform.position.y; // умножили на 10, чтобы работать с более крупными числами по оси x
        Degree_y = (int)pos_y + 90;


        str = Degree_x.ToString() + "," + Degree_y.ToString();//записываем гповорот первого и второго мотора в строку str
        //str = Degree_x.ToString();//записываем гповорот первого и второго мотора в строку str
        Debug.Log(str);
        // управление мотором
        if (sp.IsOpen)
        {
            sp.WriteLine(str);
            //print(str);
        }

    }
}
