﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class TrigonometryShit : MonoBehaviour
{
    public GameObject Cube;
    public GameObject Point; //Точка для просчета горизонтального угла
    //Point B1, B2; // Точки B1 - Старая , B2 - Новая
    float B1C1 = 3, A2B2 = 4, B2C2, A2C2 = 5; //Стороны
    float sideA,sideB,sideC;
    public float a, a2; //Углы
    Vector3 p; // Точка для расчета горизонтального движения
    Vector3 pOld;
    Vector3 currentCoordinates;
    Vector3 oldCoordinates;

    private void Start()
    {
        oldCoordinates = currentCoordinates = Cube.GetComponent<Transform>().position;
        pOld = Point.GetComponent<Transform>().position;
    }

    private void Update()
    {
        p = Point.GetComponent<Transform>().position;
        currentCoordinates = Cube.GetComponent<Transform>().position;
        //Вертикальное движение
        // Длина нового отрезка
        B2C2 = B1C1 + Mathf.Sqrt((currentCoordinates.x - oldCoordinates.x) *
            (currentCoordinates.x - oldCoordinates.x) +
            (currentCoordinates.y - oldCoordinates.y) *
            (currentCoordinates.y - oldCoordinates.y));
        //Считаем косинусы и переводим его в угол
        a = Mathf.Acos((B2C2 * B2C2 + A2C2 * A2C2 - A2B2 * A2B2) / (2 * B2C2 * A2C2)) * 180 / Mathf.PI;
        //Горизонтальное движение
        //Длина новых отрезков формулой 
        sideA = Mathf.Sqrt((currentCoordinates.x-p.x)*(currentCoordinates.x - p.x)+ (currentCoordinates.y - p.y)* (currentCoordinates.y - p.y));
        sideB = Mathf.Sqrt((oldCoordinates.x-currentCoordinates.x)*(oldCoordinates.x - currentCoordinates.x) + (oldCoordinates.y - currentCoordinates.y)* (oldCoordinates.y - currentCoordinates.y));
        sideC = Mathf.Sqrt((p.x-oldCoordinates.x)*(p.x - oldCoordinates.x)+ (p.y - oldCoordinates.y)* (p.y - oldCoordinates.y));
        //Поиск углов 
        a2 = Mathf.Acos((sideA*sideA + sideC*sideC - sideB*sideB) /(2* sideA*sideC)) * 180 / Mathf.PI;
        p = Point.GetComponent<Transform>().position;
        Debug.Log(a2);
        pOld = p;
        oldCoordinates = currentCoordinates;
        B1C1 = B2C2;
    }
}