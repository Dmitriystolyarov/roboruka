#include <Wire.h>
#include <LiquidCrystal_I2C.h>

LiquidCrystal_I2C lcd(0x27, 16, 2); // set the LCD address to 0x27 for a 16 chars and 2 line display

void setup()
{
  lcd.init();
  lcd.backlight();
  lcd.setCursor(0, 0);
  Serial.begin(9600);
}

float* ReadSerial()
{
  String source = "";
  float vector[3];
  int i = 0;
  while (Serial.available())
  {
    char symbol = (char)Serial.read();
    else if (symbol == 32)
    {
      vector[i] = source.toFloat();
      source = "";
      i++;
    }
    else if (symbol >= 48 && symbol <= 57 || symbol == 44)
    {
      if (symbol == 44)
      {
        source += ".";
      }
      else
      {
        source += symbol;
      }
    }
  }
  return vector;
}

void loop()
{
  float* vector = ReadSerial();
  for (int i = 0; i < 3; i++)
    lcd.print(vector[i]);
  delay(1000);
  lcd.clear();
}
